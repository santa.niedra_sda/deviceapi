package com.example.network.Utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DeviceTypeEnum {
    GATEWAY,
    SWITCH,
    ACCESS_POINT;

    public static DeviceTypeEnum getFromString(String deviceType){
        if(deviceType.equals("gateway")){
            return GATEWAY;
        }else if(deviceType.equals("switch")){
            return SWITCH;
        }else{
            return ACCESS_POINT;
        }
    }
}

