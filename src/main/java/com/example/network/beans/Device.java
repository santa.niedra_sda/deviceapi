package com.example.network.beans;

import com.example.network.Utils.DeviceTypeEnum;
import lombok.*;

@Data
@AllArgsConstructor
public class Device {
    private DeviceTypeEnum deviceType;
    private String macAddress;
    private String upLinkMacAddress;
}
