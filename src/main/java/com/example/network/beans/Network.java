package com.example.network.beans;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class Network {
    Map<String, Device> deviceMap = new HashMap<>();

}
