package com.example.network.resources;

import com.example.network.services.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/device")
public class DeviceResource {
    @Autowired
    private DeviceService deviceService;

    @POST
    @Path("/registerDevice/{deviceType}/{macAddress}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registerDevice(@PathParam("deviceType") String deviceType, @PathParam("macAddress") String macAddress, @QueryParam("upLinkMacAddress") String upLinkMacAddress){
        deviceService.registerDevice(deviceType, macAddress, upLinkMacAddress);
        return Response.ok("device was registered successfully").build();
    }

    @GET
    @Path("/orderedByType")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getDeviceListOrderedByType(){
        return Response.ok(deviceService.getDeviceListOrderedByType()).build();
    }

    @GET
    @Path("/getDevice/{macAddress}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getDeviceByMacAddress(@PathParam("macAddress") String macAddress){
        return Response.ok(deviceService.getDeviceByMacAddress(macAddress)).build();
    }

    @GET
    @Path("/getTopologyNetwork")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getTopologyNetwork(){
        return Response.ok(deviceService.getTopologyNetwork()).build();
    }

    @GET
    @Path("/getTopologyNetwork/{macAddress}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getTopologyNetworkByDeviceMacAddress(@PathParam("macAddress") String macAddress){
        return Response.ok(deviceService.getTopologyNetwork(macAddress)).build();
    }

}
