package com.example.network.services;

import com.example.network.Utils.DeviceTypeEnum;
import com.example.network.beans.Device;
import com.example.network.beans.Network;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
public class DeviceService {
    private static final List<Network> networkList = new ArrayList<>();


    public void registerDevice(String deviceType, String macAddress, String upLinkMacAddress) {
        Device newDevice = new Device(DeviceTypeEnum.getFromString(deviceType), macAddress, upLinkMacAddress);

        Network deviceNetwork;

        if (newDevice.getUpLinkMacAddress() == null && !networkDuplicateController(newDevice.getMacAddress())) {
            deviceNetwork = new Network();
            deviceNetwork.getDeviceMap().put(newDevice.getMacAddress(), newDevice);
            networkList.add(deviceNetwork);
        } else {
            networkList.forEach(network -> {
                Map<String, Device> deviceMap = network.getDeviceMap();

                if (deviceMap.containsKey(upLinkMacAddress)) {
                    deviceMap.put(newDevice.getMacAddress(), newDevice);
                } 
            });
        }
    }


    public List<Device> getDeviceListOrderedByType() {
        List<Device> result = new ArrayList<>();

        networkList.forEach(network -> {
            result.addAll(network.getDeviceMap().values());
        });

        result.sort(Comparator.comparing(Device::getDeviceType));

        return result;
    }

    public Device getDeviceByMacAddress(String macAddress) {
        List<Device> result = new ArrayList<>();

        networkList.forEach(network -> {
            Map<String, Device> deviceMap = network.getDeviceMap();
            if (deviceMap.get(macAddress) != null) {
                result.add(deviceMap.get(macAddress));
            }
        });

        return result.isEmpty() ? null : result.get(0);
    }

    public List<Network> getTopologyNetwork() {
        return networkList;
    }

    public Network getTopologyNetwork(String macAddress) {
        List<Network> result = new ArrayList<>();

        networkList.forEach(network -> {
            Map<String, Device> deviceMap = network.getDeviceMap();

            if (deviceMap.containsKey(macAddress)) {
                result.add(network);
            }
        });

        return result.isEmpty() ? null : result.get(0);
    }

    public boolean networkDuplicateController(String macAddress) {

        AtomicBoolean isDuplicate = new AtomicBoolean(false);

        networkList.forEach(network -> {

            if (network.getDeviceMap().containsKey(macAddress)) {
                isDuplicate.set(true);
            }
        });
        return isDuplicate.get();
    }
}
